const fs = require('fs')
const path = require('path')
const spawn = require('child-process-promise').spawn

if (process.argv.length <= 2) {
    console.log(`Usage: node ${path.basename(__filename)} imageDirectory`)
    process.exit(-1)
}

const imageDirectory = process.argv[2]

fs.readdir(imageDirectory, (err, files) => {
    if (err) {
        console.log(err)
    } else {
        files.forEach((f) => {
            let filePath = path.join(imageDirectory, f)

            // Test file for known image format.
            // magick identify image.png
            spawn('magick', ['identify', filePath], { capture: ['stdout', 'stderr'] })
                .then((result) => {
                    console.log(`Processing ${filePath}...`)

                    // Get image width and height
                    // magick convert image.png -format "%wx%h" info:
                    return spawn('magick', ['convert', filePath, '-format', '%wx%h', 'info:'], {
                        capture: ['stdout', 'stderr']
                    })
                })
                .then((result) => {
                    let imageSize = result.stdout.toString().split('x')
                    let imageWidth = parseInt(imageSize[0])
                    let imageHeight = parseInt(imageSize[1])
                    let squareSize =
                        imageWidth < imageHeight
                            ? `${imageHeight}x${imageHeight}`
                            : `${imageWidth}x${imageWidth}`

                    let newImageFile = appendToFilename(filePath, '_square')

                    console.log(
                        `Adjusting ${filePath} from ${imageWidth}x${imageHeight} to ${squareSize}`
                    )

                    // Expand image to have equal width and height.
                    // magick convert image.png -gravity center -extent 550x550 image_square.png
                    return spawn(
                        'magick',
                        [
                            'convert',
                            filePath,
                            '-gravity',
                            'center',
                            '-extent',
                            squareSize,
                            newImageFile
                        ],
                        {
                            capture: ['stdout', 'stderr']
                        }
                    )
                })
                .then((result) => {
                    console.log(`Processing ${filePath} complete.`)
                })
                .catch((err) => {
                    console.log(`ERROR: Error processing file ${filePath}.`)
                    console.log(err.stderr)
                })
        })
    }
})

let appendToFilename = (filename, string) => {
    let dotIndex = filename.lastIndexOf('.')
    if (dotIndex == -1) {
        return filename + string
    } else {
        return filename.substring(0, dotIndex) + string + filename.substring(dotIndex)
    }
}
