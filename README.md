# image_squarer

A Node script using ImageMagick to extend images to have equal width and
height.

## Usage

1. Install [Node](https://nodejs.org/en/download/).
2. Install [ImageMagick](https://imagemagick.org/script/download.php)
3. Clone this repo.
4. Run `npm install`.
5. Run `node image_squarer.js imageDirectory`.

## License

This project is licensed under the terms of the [ISC license](/LICENSE).
